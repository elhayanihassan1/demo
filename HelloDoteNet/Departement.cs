﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDoteNet
{
    class Departement
    {

        #region ATTRIBUTS
        private string nom;
        private string nemuro;

        #endregion

        #region CONSTRUCTEURS
        public Departement(string nom, string nemuro)
        {
            this.nom = nom;
            this.nemuro = nemuro;
        }



        #endregion

        #region PROPERTIES
        public string Nom { get; set; }
        public string Nemuro { get; set; }
        #endregion
    }
}
