﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDoteNet
{
    class Program
    {
        static void Main(string[] args)
        {
            string val = "52";

            int x = int.Parse(val);
            Console.WriteLine("Hello .NET");

            IList<Departement> depts = new List<Departement>();

            depts.Add(new Departement("creuse", "23"));
            depts.Add(new Departement("Val de Marne", "94"));
            depts.Add(new Departement(" Finistère", "29"));

            int nbElement = depts.Count;// property ( get)

            Departement d = depts[2]; // indexeur
            Ville v = new Ville();

         
            v.Superficie = 105.4;
            v.Population = 20000000;
            Console.WriteLine(v.Densite);

            Console.ReadLine();

        }
    }
}
