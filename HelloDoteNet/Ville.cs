﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDoteNet
{
    class Ville
    {

        #region ATTRIBUTS

        private string nom;
        private int population;
        private double superficie;

        #endregion

        
        #region AUTOPROPERTIES 
        public string Nom { get; set; }
        public int Population { get; set; }

        public double Superficie { get; set; }
        public Departement Departement { get; set; }


        #endregion

        #region CONSTRUCTEURS

        public Ville(string nom, int population, double superficie , Departement Departement)
                {
                    this.nom = nom;
                    this.population = population;
                    this.superficie = superficie;
                    this.Departement = Departement;
                }
                public Ville() 
                {
                    
                }


        #endregion

        #region METHODES
        public double Densite => population / superficie;
        #endregion
    }
}
