﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.AFCEPF.IntroCSharp.MesOutils;

namespace Fr.AFCEPF.IntroCSharp.Tournoi
{
    class Program
    {
        static void Main(string[] args)
        {

               // 1 - ajouter une réference à l'assembly désiré
               // 2-  vérifier que la classe à utiliser est bien public
            int annee = 2004;

            Calendrier c = new Calendrier();

            Console.WriteLine(" L'annee   {0} {1} {2} {3} bissextille",
                                     "{" , annee , "}",
                                      (c.EstBissextile(annee)) ? "est" : "n'est pas");
           
            Console.ReadLine();
             
        }
    }
}
